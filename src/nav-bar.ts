import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

export class NavBar {
  @bindable router: Router;
  menu: HTMLDivElement;
  hamburgerButton: HTMLDivElement;
  toggleMenu() {
    this.menu.classList.toggle('is-active');
    this.hamburgerButton.classList.toggle('is-active');
  }
}
