import {Aurelia} from 'aurelia-framework';
import {Router, RouterConfiguration} from 'aurelia-router';
import {PLATFORM} from 'aurelia-pal';

export class App {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router) {
    config.title = 'Aurelia';
    config.map([
      { route: ['', 'erbjudanden'], name: 'computers', moduleId: PLATFORM.moduleName('./computers/computer-gallery'), nav: true, title: 'Erbjudanden' },
    ]);

    this.router = router;
  }
}
