export class Computer {
    done = false;

    constructor(
        public title: string,
        public img: string,
        public description: string,
        public url: string) { }
}
