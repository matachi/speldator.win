import {Computer} from './computer';

export class ComputerGallery {
  computers: Computer[] = [];

  constructor() {
    import('./data').then(list => {
      for (var item of list.default) {
        this.computers.push(new Computer(
          item.title, item.img, item.description, item.url));
      }
    });
  }
}
