all: env crawl

env:
	python3 -m venv env
	./env/bin/pip install scrapy

crawl:
	rm -f src/computers/data.ts
	./env/bin/scrapy crawl --output src/computers/data.ts --output-format=js webhallen
