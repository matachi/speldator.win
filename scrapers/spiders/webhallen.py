# -*- coding: utf-8 -*-
import scrapy
from scrapy.loader import XPathItemLoader
from scrapy.loader.processors import Join


class WebhallenSpider(scrapy.Spider):
    name = 'webhallen'
    allowed_domains = ['webhallen.com']
    start_urls = ['https://www.webhallen.com/se-sv/datorer_och_tillbehor/stationara_datorer/gaming/']

    def parse(self, response):
        for product_link in response.css('.productlist .prod_list_row a'):
            yield response.follow(product_link, self.parse_product)

        for next_page in response.css('.next > a'):
            yield response.follow(next_page, self.parse)

    def parse_product(self, response):
        title = response.css('[itemprop=name]::text').extract_first()
        img = response.css('#prod_image::attr(src)').extract_first()
        loader = XPathItemLoader(response=response)
        #description = HtmlXPathSelector(response).select("//div[@id='prod_tab']/text()").extract_first()
        description = loader.get_xpath("//div[@id='prod_tab']/node()", Join())
        yield {'title': title, 'img': img, 'description': description, 'url': response.url}
